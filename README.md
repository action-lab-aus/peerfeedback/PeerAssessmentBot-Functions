# PeerAssessmentBot-Functions

This repository contains the Firebase Cloud Functions which facilitates the pro-active prompts for Peer Assessment Bot when the bot has not received any message from the user after a pre-defined timeout.

Peer Assessment Bot is a system developed to facilitate the peer feedback process with a task-oriented chatbot. The Peer Assessment Bot offers the opportunity for students at Monash to provide and receive anonymous feedback on the draft videos that they produced for FIT4005/FIT5125/FIT5143 Research Methods in Information Technology, via a chat interface. Throughout the peer-assessment process, students will have the chance to take a look at other students’ works-in-progress, and get an understanding of the potential improvements that they can make for the final submission.

## Getting Started

Please set up the following credentials before deploying the functions:

```JSON
"project_name": {
    "base_channel_url": "https://YOUR_BASE_CHANNEL_URL/api/notify",
    "assess_phase": "PEER_ASSESSMENT_INTAKE",
    "base_directline_url": "https://YOUR_DIRECTLINE_CHANNEL_URL/notify"
}
```

The following command can be used to set up the credentials for the functions:

```
firebase functions:config:set project_name.base_channel_url="https://YOUR_BASE_CHANNEL_URL/api/notify"
```

You can then deploy any updated Firebase Functions, config variables and Rules to the live version of that environment with:

```
(All changes) firebase deploy
(Only Functions and config changes) firebase deploy  --only functions
```
