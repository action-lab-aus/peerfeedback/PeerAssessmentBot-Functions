const functions = require("firebase-functions");
const admin = require("firebase-admin");
const axios = require("axios");
const environment = functions.config()[process.env.GCLOUD_PROJECT];

admin.initializeApp();

const db = admin.firestore();
const idleRef = db.collection(`${environment.assess_phase}/Idle/Messages`);

exports.idleScheduler = functions.pubsub
  .schedule("every 1 minutes")
  .timeZone("Australia/Melbourne")
  .onRun(async (context) => {
    try {
      const snapshot = await idleRef.get();
      const promises = [];

      snapshot.forEach((doc) => {
        // console.info(doc.id, "=>", doc.data());

        const channel = doc.data().Channel;
        const userId = doc.data().User_Id;
        const message = doc.data().Message;
        const createdAt = doc.data().Created_At.toDate();
        const currentTime = new Date(Date.now());
        // Time diff in seconds
        const diff = (currentTime - createdAt) / 1000;
        console.log("Idle Prompt Diff: ", diff);

        if (diff >= 120) {
          const url =
            channel === "DIRECTLINE"
              ? environment.base_directline_url
              : environment.base_channel_url;

          // Send the idle prompts
          promises.push(
            axios({
              method: "POST",
              headers: {
                "Content-Type": "application/json",
              },
              url: url,
              data: {
                type: "message",
                channel: channel,
                userId: userId,
                message: message,
                type: "idle",
              },
            })
          );

          // Remove the idle message node
          promises.push(idleRef.doc(doc.id).delete());
        }
      });

      await Promise.all(promises);
    } catch (error) {
      console.error("Error: ", error);
    }
  });
